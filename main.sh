#! /bin/bash

#INIT
cd "$(dirname "${BASH_SOURCE[0]}")"
params="$(echo "$@" | sed 's|'"$1"'||1' 2> /dev/null)"

#LIBRARY
. ./library.sh

#FUNCTIONS
runmw () {
	./mw.sh $params
}
applymodset () {
	moddir="./modules/"
	modset="$1"
	. ./scripts/modulemgr/modset.sh
}
showhelp () {
	sed -e '1,/#\HELPPART/d' "$0"
}
readme () {
	mdp ./README.md ||
	less ./README.md ||
	cat ./README.md
}
showlicense () {
	less ./LICENSE
}

#MAIN
case "$1" in
	start) runmw
	;;
	modset) applymodset "$2"
	;;
	license) showlicense
	;;
	readme) readme
	;;
	*) showhelp
	;;
esac
exit

#HELPPART
      COMMAND LINE OVERVIEW
–––––––––––––––––––––––––––––––––
Command |     Usage     | Description
start   | <command>     | Start Magicwrap
modset  | <modset>      | Apply a module set
––––––––– Documentation –––––––––
help    |               | Show this help
readme  |               | Show readme
license |               | Show license
