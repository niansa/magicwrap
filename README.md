# Magicwrap

This is a module-based namespace wrapper slightly inspired by magisk. It is completely written in bash and does not require root access.

**Note:** Magicwrap is still in early development. An installer for module files (.zip, .tar, ...) is in paused progress.

## How to run?
1. Install bubblewrap, bash, busybox-static and optionally fakeroot if you want to use the `fakeroot` module.
2. Run `./main.sh start`

## Usage
`[mwdebug=true] [mwcinit=/path/to/init.sh] [mwcconf=/path/to/config.sh] ./main.sh <command>`

## Default settings
By default, Magicwrap is optimized to run simple terminal applications. Disable or enable modules in modules/{config,init}.sh to make Magicwrap fit your needs.
Always enable/disable modules in **both** init.sh and config.sh!!!

## Module sets
Another way to fit magicwrap to your needs is to apply a module set by running: `./main.sh modset <modset>`
* **Every module** restricts access to `/etc` and `/var`
1. **simple**: Allows access to most directorys
2. **complete**: Restricts access to `/home` and `/opt`
3. **graphical**: Allows access to users home directory and some directorys to run graphical applications properly
4. **graphical-adv**: Same as graphical but restricts access to `/home` and `/opt` and enables controller

## Module overview
1. **base**: Creates base filesystem and init
2. **advdirs**: Mounts more directorys (/opt, /srv)
3. **jailedcfg**: Mounts stock debian configuration directorys (/etc, /var)
4. **sharedfs**: Mounts directorys that are shared between namespaces (/home, /opt)
5. **graphical**: Mounts some files that are required to run graphical applications properly
5. **fakeroot**: Replaces sudo with fakeroot
6. **control**: Allows application to do advanced operations (umount, external xdg-open)
8. **pidone**: Runs application as PID 1; not recommended

## Module management
The module management scripts are inside `scripts/modulemgr/` but they are not recommended yet as most modules will throw errors.
