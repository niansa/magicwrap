##! /bin/bash

#FUNCTIONS
cleanup () {
	debugout 'I: Cleaning up...'
	rm -rf "$sessiondir"
	rm -rf "$controldir"
}

#LIBRARY
. ./library.sh

#INIT
debugout 'I: Constructing environment...'
cd "$(dirname "${BASH_SOURCE[0]}")"
defaultshell="/bin/bash"
params="$@"
basedir="$(pwd)"
session="$RANDOM"
sessiondir="/tmp/rootfs${session}"
controldir="/tmp/control${session}"
mkdir "$sessiondir" &&
mkdir "$controldir" ||
(errout 'E: Failed to construct environment, aborting...' ; exit 22) || exit 1
export session

#CHECK
debugout 'I: Getting runpath...'
runpath="${basedir}/$0"
(ls "$runpath" &> /dev/null) ||
(errout 'E: Runpath could not be determined, aborting...' ; cleanup ; exit 22) || exit 2

#MODULE-CUSTOM
test -v mwcconf ||
export mwcconf="./config/custom.sh"
test -v mwcinit ||
export mwcinit="./init/custom.sh"

#MODULE-INIT
debugout 'I: Initalizing modules...'
cd ./modules/
. ./init.sh
cd "$basedir"

#MODULE-LOAD
cd ./modules/
debugout 'I: Loading modules...'
bwrap_rawconfig="$(./config.sh)"
bwrap_config="$(echo "$bwrap_rawconfig" | tr ' \\\n' ' ' |
sed 's|((runpath))|'"$runpath"'|g' |
sed 's|((controldir))|'"$controldir"'|g' |
sed 's|((home))|'"$HOME"'|g' |
sed 's|((mwdirs))|'"$HOME"'/.mwdirs|g' |
sed 's|((inhome))|/home/'"$USER"'|g' |
sed 's|((user))|'"$USER"'|g' |
sed 's|((basedir))|'"$basedir"'|g')"
cd "$basedir"

#PASSED
debugout 'I: Checking parameters...'
if [[ "$params" == "" ]]; then
	debugout 'W: No paramerers passed! "'"$defaultshell"'" will be executed.'
	passed="$defaultshell"
else
	passed="$@"
fi
export passed

#RUN
debugout 'I: Running bubblewrap...'
cd ./modules/
bwrap \
--bind "$sessiondir" / \
--bind "$sessiondir" /rootfs \
$bwrap_config \
--tmpfs "$basedir" \
--ro-bind "$basedir"/library.sh /mwlib.sh \
/init "$passed" ||
debugout 'W: Some error caused bubblewrap to terminate: '"$?"

cleanup
