if [ ! -d ~/.sharedfs/home ]; then
	debugout 'I: Preparing sharedfs...'
	mkdir -p ~/.sharedfs/opt
	cp -r /etc/skel ~/.sharedfs/home
fi
