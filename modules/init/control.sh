debugout 'I: Preparing controldir...'
mkdir "$controldir"/{shared,ro}
touch "$controldir"/{cmd,stdout,errout,ro/.lock}

debugout 'I: Starting controlling daemon...'
setsid ./data/control/daemon.sh "$controldir" "$sessiondir" "$basedir" &

debugout 'I: Telling init to start entrypoint...'
export doextpid=true
