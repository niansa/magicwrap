if [ ! -d ~/.sharedfs/lib64 ]; then
	debugout 'I: Preparing sharedfs...'
	mkdir -p ~/.sharedfs/{bin,sbin,etc,var,lib,lib64}
fi
