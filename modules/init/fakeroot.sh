if [ ! -e "/usr/bin/fakeroot" ]; then
	errout 'E: fakeroot could not be found at /usr/bin/fakeroot but fakeroot module is activated, aborting...'
	exit 35
fi
