#! /busybox/ash

if [ "$1" = "" ]; then
	echo "Syntax: funlock <file to unlock>"
elif [ ! -e "$1" ]; then
	echo "<$1> does not exist"
elif [ ! -r "$1" ]; then
	echo "<$1> is not readable"
else
	echo "Unlocking <$1>..."
fi


(cp -r "$(readlink -f "$1")" /tmp/funlock$$ &&
rm -f "$1" &&
cp -Hr /tmp/funlock$$ "$1" &&
rm -rf /tmp/funlock$$) ||
echo "Error: <$1> can not be unlocked. Is it already unlocked? Try to unlock its parent directory."
