export controldir="$1"
export sessiondir="$2"
export basedir="$3"

until [ -f "$sessiondir"/entrypoint ]; do
	sleep 0.2
done
export extpid="$(ps -a -o ppid,args | grep "/entrypoint $session" | grep -v "grep" | sed -r 's/^([^.]+).*$/\1/; s/^[^0-9]*([0-9]+).*$/\1/')"
rm "$sessiondir"/entrypoint

cd "$basedir"

while (ls "$controldir" > /dev/null 2> /dev/null); do
	touch "$controldir"/cmd
	passedcmd=$(cat "$controldir"/cmd)
	if [ "$passedcmd" != "" ]; then
		(./modules/data/control/cmd.sh $(cat "$controldir"/cmd)) > "$controldir"/stdout 2> "$controldir"/errout
		echo -n > "$controldir"/cmd
	fi
	sleep 1
done
