#! /bin/bash


desturl="$1"

zenity --question --cancel-label="Secured" --ok-label="Unsecured" --text 'How to open <b>'"$desturl"'</b>?' 2> /dev/null
if [ "$?" = "1" ] && [ -x /usr/bin/xdg-open ]; then
	/usr/bin/xdg-open "$1" &&
	exit
	zenity --waring --text 'Failed to open <b>'"$desturl"'</b> securely.' ||
	exit
fi

incontrol open "$desturl"
