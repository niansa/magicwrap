#! /bin/bash

#FUNC:REQDIALOG
function reqdialog {
	zenity --question --title "Software request" --ok-label="Allow" --cancel-label="Deny" --text "$@"
}
#FUNC:GETV
function getv {
	test -f "${controldir}/ro/${1}" &&
	cat "${controldir}/ro/${1}"
}
#FUNC:SETV
function setv {
	echo "$2" > "${controldir}/ro/${1}"
}

#FUNC:DELV
function delv {
	rm "${controldir}/ro/${1}"
}

#INIT
status="OKAY"
params="$(echo "$@" | sed 's|'"$1"' ||1')"

#CMD:TEST
if [ "$1" = "test" ]; then
	reqdialog "software request dialog test" ||
	status="FAILED"

#CMD:GETVAR
elif [ "$1" = "getvar" ] && [ "$2" != "" ]; then
	status="$(getv $2)" ||
	status="FAILED"

#CMD:UMOUNT
elif [ "$1" = "umount" ] && [ "$2" != "" ]; then
	passed="$(echo "${sessiondir}/$2" | sed 's|\.\.||g')"
	rmdir "$passed" && mkdir "$passed" ||
	status="FAILED"

#CMD:OPEN
elif [ "$1" = "open" ] && [ "$2" != "" ]; then
	passed1="$(echo "$params" | sed 's|file://|file:///proc/'"$extpid"'/root|1')"
	if [[ "$2" == /* ]]; then
		passed="$(echo "$params" | sed 's|/|file:///proc/'"$extpid"'/root/|1')"
	else
		passed="$passed1"
	fi
	reqdialog 'Allow software to open <b>'"${params}"'</b>?' &&
	xdg-open "$passed" ||
	status="FAILED"

#CMD:DEBUG
elif [ "$1" = "debug" ] && [ "$mwdebug" = "true" ]; then
	($params) ||
	status="FAILED"

#CMD:HELP
elif [ "$1" = "help" ]; then
	echo 'Command - X required? - Description'
	echo 'test    -      yes    - test command'
	echo 'umount  -      no     - gracefully umount <directory>'
	echo 'open    -      yes    - request to open <URL>'
	echo '------ advanced commands ------'
	echo 'getvar  -      no     - read a variable/property'
	echo 'debug   -      no     - run command directly, debugmode only'
	status=''

#CMD:UNKNOWN
else
	status="BAD"
fi

#RETURN
echo "$status"
