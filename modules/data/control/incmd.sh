#! /busybox/ash

status=""
echo > /control/stdout
echo "$@" > /control/cmd

until [ "$status" != "" ]; do
	status="$(cat /control/stdout || echo 'INCMDFAILED')"
	sleep 0.5
done

echo "$status"
if [ "$status" != "OKAY" ]; then
	exit 1
else
	exit 0
fi
