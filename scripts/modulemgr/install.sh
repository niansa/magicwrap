#! /bin/bash

#INITVARS
mgrdir="/tmp/${USER}mwmodmgr"
moduledir="$1"

#CHECKMGR
if [ -d "$mgrdir" ]; then
	echo 'E: Module manager is already running, aborting...'
	exit 1
elif [ ! -r "$2" ]; then
	echo 'E: The module could not be found, was not specified or is not readable, aborting...'
	exit 2
fi

#INITDIRS
mkdir "$mgrdir"
mkdir "$mgrdir"/unpacked/

#COPY
echo 'I: Copying module...'
cp "$2" "$mgrdir"/module.zip
cd "$mgrdir"

#UNPACK
echo 'I: Unpacking module...'
cd ./unpacked/
unzip -q ../module.zip

#READ
echo 'I: Reading module...'
. ./module.sh

#CHECKMODULE
echo 'I: Checking module...'
if [ -e "${moduledir}/data/${modid}/" -o -e "${moduledir}/config/${modid}.sh" -o -e "${moduledir}/init/${modid}.sh" ]; then
	echo 'W: Module is already installed, updating it...'
	ainstalled="yes"
	(rm -rf "${moduledir}/data/${modid}"
	rm "${moduledir}/config/${modid}.sh"
	rm "${moduledir}/init/${modid}.sh") 2> /dev/null
fi

#INSTALL
echo 'I: Installing module...'
(cp -r ./data "${moduledir}/data/${modid}"
cp ./config.sh "${moduledir}/config/${modid}.sh"
cp ./init.sh "${moduledir}/init/${modid}.sh") 2> /dev/null

#ACTIVATE
if [ ! "$ainstalled" = "yes" ]; then
	echo 'I: Activating module...'
	echo "cat '${moduledir}/config/${modid}.sh'" >> "$moduledir"/config.sh
	echo ". '${moduledir}/init/${modid}.sh'" >> "$moduledir"/init.sh
fi

#CLEAN
echo 'I: Cleaning up...'
rm -rf "$mgrdir"
