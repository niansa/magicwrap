#! /bin/bash

#INITVARS
mgrdir="/tmp/${USER}mwmodmgr"
moduledir="$1"
modid="$2"

#CHECKMGR
if [ -d "$mgrdir" ]; then
	echo 'E: Module manager is already running, aborting...'
	exit 1
fi

echo "mgrdir=$mgrdir
moduledir=$moduledir
modid=$modid"

#CHECKMODULE
echo 'I: Checking module...'
#if [ ! -e "${moduledir}/data/${modid}/" -o -e "${moduledir}/config/${modid}.sh" -o -e "${moduledir}/init/${modid}.sh" ]; then
#	echo 'E: Module does not seem to be installed, aborting...'
#	rm -rf "$mgrdir"
#	exit 2
#fi

#PURGE
echo 'W: Purging module...'
(rm -rf "${moduledir}/data/${modid}/"
rm "${moduledir}/config/${modid}.sh"
rm "${moduledir}/init/${modid}.sh") 2> /dev/null

#CLEAN
echo 'I: Cleaning up...'
rm -rf "$mgrdir"
