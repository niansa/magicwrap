#! /bin/bash

#CHECK:LIB
debugout 'I: This is a library test!'
if [ "$?" = "127" ]; then
	exit 742
fi

#INIT
modsetdir="${moddir}/sets/${modset}/"
debugout "I: moddir = $moddir"
debugout "I: modset = $modset"
debugout "I: modsetdir = $modsetdir"

#CHECK:EXISTENCE
if [ ! -d "$modsetdir" ]; then
	errout 'E: This modset could not be accessed, aborting...'
	exit 743
fi

#APPLY
debugout 'I: Applying modset...'
cp "${modsetdir}/config.sh" "${moddir}/" &&
cp "${modsetdir}/init.sh" "${moddir}/" ||
errout 'E: Failed to apply modset!'
